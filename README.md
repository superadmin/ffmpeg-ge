# Ffmpeg GE

Procesa los archivos desde GE

## Funcionamiento

Crear un "Spaces" en DigitalOcean, y setear en ambos proyectos las mismas credenciales (o diferentes, pero con acceso al mismo "bucket")

GE:

```env
GUIAESCORT_URL_PUSH_VIDEOS=http://{ruta}/api/video-nuevo

DIGITALOCEAN_SPACES_KEY={digitalocean}
DIGITALOCEAN_SPACES_SECRET={digitalocean}
DIGITALOCEAN_SPACES_DIRECTORY={digitalocean}
```

FFmpeg:

```env
DIGITALOCEAN_SPACES_KEY={digitalocean}
DIGITALOCEAN_SPACES_SECRET={digitalocean}
```

## Cómo funciona

1) GE: Se declara una URL de retorno, así:

```php
Route::get('api/actualizar-reels/{escort:id}', function (Escort $escort) {
    $url = request('url');
    
    return $url;
})->name('ffmpeg.video.store'); // <- Esta ruta
```

2) GE: Se envía desde GE, con la URL de retorno

```php
$notificar = Http::get(env('GUIAESCORT_URL_PUSH_VIDEOS'), [
    'url' => $subir,
    'return_url' => route('ffmpeg.video.store', $escort),
])->body();
```

### Uso:

1) Se sube el vídeo en GE (y "avisa" automáticamente por el return_url)

2) Se abre la URL de procesar

## Comandos disponibles

```sh
php artisan procesar:clip # Procesa el primer clip disponible con FFmpeg
php artisan subir:clip # Sube el primer clip disponible a Spaces de DO
php artisan borrar:carpetas # Borrar carpetas de entrada y salida, con su contenido
```
