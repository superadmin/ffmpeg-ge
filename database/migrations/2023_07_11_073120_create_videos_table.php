<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->id();
            $table->string('url');
            $table->string('nombre');
            $table->string('return_url');
            $table->string('entrada_local')->nullable();
            $table->string('salida_local')->nullable();
            $table->string('md5')->nullable();

            $table->datetime('procesando_clip_desde')->nullable();
            $table->datetime('procesando_clip_hasta')->nullable();

            $table->datetime('procesando_gif_desde')->nullable();
            $table->datetime('procesando_gif_hasta')->nullable();

            $table->string('clip_salida_local')->nullable();
            $table->datetime('clip_subido_el')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('videos');
    }
};
