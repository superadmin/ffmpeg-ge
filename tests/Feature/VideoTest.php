<?php

namespace Tests\Feature;

use App\Models\Video;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class VideoTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function puede_guardar_campos_al_marcar_como_empezar()
    {
        $video = Video::factory()->create();

        $video->marcarClipComoEmpezar();

        $this->assertNotNull($video->procesando_clip_desde);
    }

    /** @test */
    public function puede_guardar_campos_al_marcar_como_terminar()
    {
        $video = Video::factory()->create();

        $video->marcarClipComoTerminar();

        $this->assertNotNull($video->procesando_clip_hasta);
    }

    /** @test */
    public function puede_procesar_clip_y_llamar_a_marcar_como_empezar_y_marcar_como_terminar()
    {
        $this->markTestIncomplete('Falta conocimento en Mockery');

        $video = Video::factory()->create();

        $video->procesarClip();

        $this->assertNotNull($video->procesando_clip_desde);
        $this->assertNotNull($video->procesando_clip_hasta);
    }

    /** @test */
    public function conoce_los_clips_listos_para_subir()
    {
        $video_ignorar = Video::factory()->create([
            'clip_salida_local' => null,
            'clip_subido_el' => null,
        ]);

        $video_contar = Video::factory()->create([
            'clip_salida_local' => 'ruta/falsa/clip.mp4',
            'clip_subido_el' => now(),
        ]);

        $this->assertCount(1, Video::clipsParaSubir()->get());
    }
}
