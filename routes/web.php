<?php

use App\Classes\GEVideo;
use App\Http\Controllers\GEVideoController;
use App\Models\Video;
use FFMpeg\Coordinate\Dimension;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\Format\Video\X264;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/clip', [GEVideoController::class, 'clip']);
Route::get('/gif', [GEVideoController::class, 'gif']);

Route::get('api/video-nuevo', function () {
    $url = Video::nuevo(request()->only('url', 'return_url'), 'pruebita');

    // dd(Storage::disk('entrada')->get($nombre));

    // dd('Listo');

    return "URL a procesar en DO: {$url}";
});

Route::get('test/return_url', function () {
    $video = Video::first();

    return Http::get($video->return_url)->getBody();
});
