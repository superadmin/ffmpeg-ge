<?php

namespace App\Classes;

use FFMpeg\Coordinate\Dimension;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\FFMpeg;
use FFMpeg\Filters\Video\ResizeFilter;
use FFMpeg\Format\Video\X264;

class GEVideo
{
    public $rutaAlmacenamiento = 'app/entrada';

    public $rutaAlmacenamientoSalida = 'app/salida';

    public $metadatos = [
        'titulo' => 'Reel descargado desde guiaescort.cl',
        'compositor' => 'guiaescort.cl',
        'artista' => 'guiaescort.cl',
        'genero' => 'Sexy',
        'comentario' => 'Descargado desde guiaescort.cl',
    ];

    public $binarios = [
        'ffmpeg' => '/opt/homebrew/bin/ffmpeg', // exec('which ffmpeg')
        'ffprobe' => '/opt/homebrew/bin/ffprobe', // exec('which ffprobe')
    ];

    public function __construct($entrada)
    {
        $this->entrada = $entrada;

        $this->setFfmpeg();
        $this->setVideo();

        return $this;
    }

    public function setFfmpeg()
    {
        $this->ffmpeg = FFMpeg::create([
            'ffmpeg.binaries' => $this->binarios['ffmpeg'],
            'ffprobe.binaries' => $this->binarios['ffprobe'],
        ]);
    }

    public function setVideo()
    {
        $this->video = $this->ffmpeg->open($this->archivoTrabajoEntrada($this->entrada));
    }

    public function archivoTrabajoEntrada($archivo)
    {
        return storage_path($this->rutaAlmacenamiento.'/'.$archivo);
    }

    public function archivoTrabajoSalida($archivo)
    {
        return storage_path($this->rutaAlmacenamientoSalida.'/'.$archivo);
    }

    public function gif(string $salida, int $segundos_inicio = 2, int $duracion = 3, int $ancho = 320, int $alto = 240)
    {
        return $this->video->gif(TimeCode::fromSeconds($segundos_inicio), new Dimension($ancho, $alto), $duracion)
            ->save($this->archivoTrabajoSalida($salida.'.gif'));
    }

    public function clip(string $salida, int $segundos_inicio = 0, int $segundos_fin = 30, int $ancho = 216, int $alto = 384)
    {
        $clip = $this->video->clip(TimeCode::fromSeconds($segundos_inicio), TimeCode::fromSeconds($segundos_fin));
        $clip->filters()->pad(new Dimension($ancho, $alto), ResizeFilter::RESIZEMODE_SCALE_HEIGHT, false);

        $clip->filters()->addMetadata([
            'title' => $this->metadatos['titulo'],
            'artist' => $this->metadatos['artista'],
            'composer' => $this->metadatos['compositor'],
            'genre' => $this->metadatos['genero'],
            'comment' => $this->metadatos['comentario'],
        ]);

        $clip->save(new X264(), $this->archivoTrabajoSalida($salida.'.mp4'));

        return $clip;
    }
}
