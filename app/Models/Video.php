<?php

namespace App\Models;

use App\Classes\GEVideo;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Video extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $guarded = [];

    public function marcarClipComoEmpezar()
    {
        return $this->update(['procesando_clip_desde' => now()]);
    }

    public function marcarClipComoTerminar()
    {
        return $this->update(['procesando_clip_hasta' => now()]);
    }

    public function procesarClip()
    {
        $this->marcarClipComoEmpezar();

        $clip = (new GEVideo($this->entrada_local))->clip($this->nombre, 0, 30, 216, 384);

        $this->marcarClipComoTerminar();
    }

    public function scopeClipsParaSubir(Builder $builder)
    {
        $builder->whereNotNull('clip_salida_local')->whereNotNull('clip_subido_el');
    }

    public static function nuevo(array $request, string $nombre)
    {
        $video = self::create([
            'url' => $request['url'],
            'nombre' => $nombre,
            'return_url' => $request['return_url'],
        ]);

        $descarga = Storage::disk('digitalocean')->get($request['url']);

        Storage::disk('entrada')->put($nombre, $descarga);

        $video->update(['entrada_local' => $nombre]);

        return $request['url'];
    }
}
