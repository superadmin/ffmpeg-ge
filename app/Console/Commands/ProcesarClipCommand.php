<?php

namespace App\Console\Commands;

use App\Classes\GEVideo;
use App\Models\Video;
use Illuminate\Console\Command;

class ProcesarClipCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'procesar:clip';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Busca y procesa el primer vídeo disponible, como clip';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        if ($un_video = Video::whereNull('procesando_clip_desde')->first()) {
            $this->info('💫 Procesando...');
            $un_video->procesarClip();
            $this->info('👌 Listo!');

            return self::SUCCESS;
        }

        $this->info('🤷‍♂️ No hay clips para procesar');
    }
}
