<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class BorrarCarpetasCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'borrar:carpetas';

    protected $folders = [
        'entradas',
        'salidas',
    ];

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Borra las carpetas de entrada y salida';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->info('💫 Borrando carpetas...');

        foreach ($this->folders as $folder) {
            Storage::deleteDirectory($folder);

            $this->info("👌 Se borró la carpeta \"{$folder}\"");
        }

        return self::SUCCESS;
    }
}
