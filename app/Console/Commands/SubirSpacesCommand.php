<?php

namespace App\Console\Commands;

use App\Classes\GEVideo;
use App\Models\Video;
use Illuminate\Console\Command;

class SubirSpacesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subir:clip';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subir clip Spaces';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        if ($un_video = Video::clipsParaSubir()->first()) {
            $this->info('💫 Subiendo...');
            // $un_video->procesarClip();
            $this->info('👌 Listo!');

            return self::SUCCESS;
        }

        $this->info('🤷‍♂️ No hay clips para subir');
    }
}
