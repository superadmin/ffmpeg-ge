<?php

namespace App\Http\Controllers;

use App\Classes\GEVideo;
use Illuminate\Http\Request;

class GEVideoController extends Controller
{
    public function setInicio()
    {
        $this->inicio = now();
    }

    public function setFin()
    {
        $this->fin = now();
    }

    public function setTiempoEjecucion()
    {
        $this->setFin();
        $this->tiempo_ejecucion = $this->inicio->diffInSeconds($this->fin);
    }

    public function getTiempoEjecucion()
    {
        $this->setTiempoEjecucion();

        echo $this->tiempo_ejecucion;
    }

    public function __construct()
    {
        $this->setInicio();
    }

    public function __destruct()
    {
        $this->setFin();
    }

    public function gif()
    {
        $gif = (new GEVideo('2.mp4'))->gif("gif", 0, 2, 100);

        echo $this->getTiempoEjecucion();

        return view('welcome');
    }

    public function clip()
    {
        $clip = (new GEVideo('2.mp4'))->clip("clip", 0, 30, 216, 384);

        echo $this->getTiempoEjecucion();

        return view('welcome');
    }
}
